# Security check

Site: <https://klavogonki.ru/>

## Forgot password

<https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html#forgot-password-service>

| Test step  | Result |
|---|---|
| Open klavogonki.ru | Ok |
| Press "Enter" button  | Ok. "Log in" window opened. |
| Press "I don't remember anything" button | Ok. Textfield for email appeared |
| Enter non-existing email and submit | Ok.  Message "A player with an address *wrong email* does not exist." appeared |
| Enter existing email | Ok. Message "To the address *correct email* an email was sent with further instructions." appeared. Email was delivered. |

### Results:
 - klavogonki.ru correctly implements password recovery in accordance with the OWASP Cheat Sheet

## Email Validation

<https://cheatsheetseries.owasp.org/cheatsheets/Input_Validation_Cheat_Sheet.html#email-address-validation>

| Test step  | Result |
|---|---|
| Open klavogonki.ru | Ok |
| Push "Enter" button | Ok. "Log in" window opened.  |
| Push "Register" button | Ok. "Register" page opened.  |
| Enter email without "@" | Ok. Warning appeared. |
| Enter email `hello--world...@gmail.com` | Not Ok! Message "To the address hello--world@gmail.com an email has been sent with a link to confirm registration." and account was registered. |

### Results:
 - klavogonki.ru has basic email validation
 - validation is not carried out carefully enough, you can enter the wrong email during registration

 ## Authorization

 <https://cheatsheetseries.owasp.org/cheatsheets/Authorization_Cheat_Sheet.html#ensure-lookup-ids-are-not-accessible-even-when-guessed-or-cannot-be-tampered-with>

| Test step | Result |
|---|---|
| Open klavogonki.ru | Ok |
| Enter into account | Ok |
| Check url if it contains some identifiers | Ok, no identifiers are exposed |
| Press button with your nickname | Ok |
| Press button "Open profile" | Ok |
| Press button "Messages" | Ok |
| Check url if it contains some identifiers | Not ok, url contains profile ID |
| Check the url of the message page without authentication | Ok. Other users cannot view private messages and only see public information about the player |

### Results:
 - klavogonki.ru provides Player ID to summary pages
 - it is impossible to get private information about a player using an ID

 